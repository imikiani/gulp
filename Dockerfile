FROM node:10.16.3-alpine

RUN npm install -g gulp

COPY ./docker-entrypoint.sh /

WORKDIR /app
ENTRYPOINT ["/docker-entrypoint.sh"]
